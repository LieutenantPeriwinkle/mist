use mist_pdk::*;
use std::io::{ErrorKind, Read, Write};
use std::net::{TcpListener, TcpStream};

struct ControlServerPlugin {
    sock: TcpListener,
    streams: Vec<TcpStream>,
    current_split: usize,
    run: Run,
}

impl MistPlugin for ControlServerPlugin {
    fn init(r: &Run) -> Self {
        let sock = TcpListener::bind("127.0.0.1:2500").unwrap();
        // we shouldn't block in update, so accept connections nonblocking
        // alternatively, we could spawn a thread to handle connections
        sock.set_nonblocking(true).unwrap();
        Self {
            sock,
            streams: vec![],
            current_split: 0,
            run: r.clone(),
        }
    }
    fn update(&mut self, u: &RunUpdate) -> StateChangeRequest {
        while let Ok(conn) = self.sock.accept() {
            conn.0.set_nonblocking(true).unwrap();
            self.streams.push(conn.0);
        }
        for c in &u.change {
            if let &StateChange::EnterSplit { idx } = c {
                self.current_split = idx;
            }
        }
        let mut buf = [0; 100];
        let mut ret = StateChangeRequest::None;
        let mut closed = vec![];
        // later connections get priority
        for (i, mut s) in self.streams.iter().enumerate() {
            let e = s.read(&mut buf);
            if let Ok(1..) = e {
                match buf[0] {
                    b'p' => {
                        ret = StateChangeRequest::Pause;
                    }
                    b's' => {
                        ret = StateChangeRequest::Split;
                    }
                    b'u' => {
                        ret = StateChangeRequest::Unsplit;
                    }
                    b'k' => {
                        ret = StateChangeRequest::Skip;
                    }
                    b'r' => {
                        ret = StateChangeRequest::Reset;
                    }
                    b'f' => {
                        ret = StateChangeRequest::NextComparison;
                    }
                    b'b' => {
                        ret = StateChangeRequest::PrevComparison;
                    }
                    // Get info on current split
                    b'g' => {
                        s.write_all(
                            format!(
                                "{},{},{},{},({},{})",
                                self.current_split,
                                self.run.splits()[self.current_split],
                                self.run.pb_times()[self.current_split].raw(),
                                self.run.gold_times()[self.current_split].raw(),
                                self.run.sum_times()[self.current_split].0,
                                self.run.sum_times()[self.current_split].1.raw(),
                            )
                            .as_bytes(),
                        )
                        .unwrap_or_else(|_| closed.push(i));
                    }
                    // get current time + split time
                    b't' => {
                        s.write_all(format!("{},{}", u.time, u.split_time).as_bytes())
                            .unwrap_or_else(|_| closed.push(i));
                    }
                    b'o' => {
                        s.write_all(format!("{}", u.offset).as_bytes())
                            .unwrap_or_else(|_| closed.push(i));
                    }
                    // status ('color')
                    b'c' => {
                        s.write_all(format!("{:?}", u.status).as_bytes())
                            .unwrap_or_else(|_| closed.push(i));
                    }
                    // quit connection
                    b'q' => {
                        closed.push(i);
                    }
                    _ => {}
                }
            } else if let Err(ref err) = e {
                if err.kind() != ErrorKind::WouldBlock {
                    closed.push(i);
                }
            }
        }
        for i in closed {
            // remove streams that we couldn't write to
            self.streams.remove(i);
        }
        ret
    }

    fn shutdown(&mut self) {
        // could do work here, but we don't need to
    }
}

generate_plugin!(ControlServerPlugin);
