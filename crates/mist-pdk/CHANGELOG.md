# Changelog
### 1.1.0
- Change plugin function names to `mist_plugin_*` for disambiguation

### 1.0.0
- Initial implementation
- FFI function boilerplate generation macro for plugin development
- Trait that contains functionality of a plugin
- Type aliases for FFI function signatures for use on host side
