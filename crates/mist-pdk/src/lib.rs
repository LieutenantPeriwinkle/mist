#[cfg(not(feature = "guest"))]
use mist_core::timer::state::{RunUpdate, StateChangeRequest};
#[cfg(not(feature = "guest"))]
use mist_core::timer::Run;
use std::sync::OnceLock;

mod build_info {
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

static VERSION: OnceLock<u16> = OnceLock::new();
/// Get the crate version that was compiled.
///
/// The first 8 bits of the return value are the major version, and second 8 are
/// the minor version. This is used so that the host can be sure that the plugin is
/// exporting FFI functions with the signatures that it expects.
pub fn pdk_version() -> u16 {
    *VERSION.get_or_init(|| {
        (build_info::PKG_VERSION_MAJOR.parse::<u16>().unwrap() & 0xFF) << 8
            | (build_info::PKG_VERSION_MINOR.parse::<u16>().unwrap() & 0xFF)
    })
}

/// Signature of FFI `init` function.
pub type InitFunc = fn(*const Run);
/// Signature of FFI `update` function.
pub type UpdateFunc = fn(*const RunUpdate) -> StateChangeRequest;
/// Signature of FFI `version` function.
pub type VersionFunc = fn() -> u16;
/// Signature of FFI `shutdown` function.
pub type ShutdownFunc = fn();

// re-export so plugins don't depend directly on core
#[cfg(feature = "guest")]
#[doc(hidden)]
pub use mist_core::timer::{
    state::{RunUpdate, SplitStatus, StateChange, StateChangeRequest},
    Run,
};

#[cfg(feature = "guest")]
/// Trait defining all of a plugin's methods.
///
/// Implement this trait on a struct to make that struct into a plugin.
pub trait MistPlugin {
    /// Called once when the plugin is created. This happens at the start of the program as well as
    /// when a new run is loaded by the user.
    fn init(run: &Run) -> Self
    where
        Self: Sized;
    /// Called once each frame during runtime of the program. Will not be called while dialog boxes are open,
    /// as no frames happen during a dialog box.
    ///
    /// Returns [StateChangeRequest::None] by default. You can return any [StateChangeRequest] to request the timer
    /// to make that change, but this does not guarantee that that will happen, based on the timer's internal state
    /// or requests made by other plugins.
    fn update(&mut self, _upd: &RunUpdate) -> StateChangeRequest {
        StateChangeRequest::None
    }
    /// Called when the plugin is destroyed. This happens at the end of the program as well as when
    /// a new run is loaded by the user. Does nothing by default.
    fn shutdown(&mut self) {}
}

#[macro_export]
#[cfg(feature = "guest")]
/// Generate plugin boilerplate code.
///
/// This macro takes the name of a struct that implements [`MistPlugin`] and generates the necessary
/// FFI boilerplate for a plugin to work.
///
/// ```no_run
/// # use mist_pdk::{generate_plugin, MistPlugin, Run};
/// struct MyPlugin;
///
/// impl MistPlugin for MyPlugin {
/// // ...
/// # fn init(_: &Run) -> Self {Self}
/// }
///
/// generate_plugin!(MyPlugin);
/// ```
macro_rules! generate_plugin {
    ($plugin:ty) => {
        thread_local! {
            static PLUGIN: ::std::cell::RefCell<::std::cell::OnceCell<$plugin>> = ::std::cell::RefCell::new(::std::cell::OnceCell::new());
        }
        #[no_mangle]
        extern "C" fn mist_plugin_init(r: *const ::mist_pdk::Run) {
            let r = unsafe { &*r };
            let plug = <$plugin as ::mist_pdk::MistPlugin>::init(r);
            PLUGIN.with(|p| {p.borrow_mut().get_or_init(move || plug);}); // don't explode if init is called multiple times by accident
        }
        #[no_mangle]
        extern "C" fn mist_plugin_update(upd: *const ::mist_pdk::RunUpdate) -> ::mist_pdk::StateChangeRequest {
            let upd = unsafe { &*upd };
            let mut ret = ::mist_pdk::StateChangeRequest::None;
            PLUGIN.with_borrow_mut(|p| if let Some(mut p) = p.get_mut() {
                ret = (p as &mut dyn ::mist_pdk::MistPlugin).update(upd);
            });
            ret
        }
        #[no_mangle]
        extern "C" fn mist_plugin_shutdown() {
            PLUGIN.with_borrow_mut(|p| if let Some(mut p) = p.get_mut() {
                (p as &mut dyn ::mist_pdk::MistPlugin).shutdown();
            });
        }
        #[no_mangle]
        extern "C" fn mist_plugin_version() -> u16 {
            ::mist_pdk::pdk_version()
        }
    };
}
