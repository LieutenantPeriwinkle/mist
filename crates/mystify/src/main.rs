use eframe::{egui, NativeOptions};
use egui_extras::{Column, TableBuilder};
use mist_core::{
    dialogs::{fail, get_file, get_save_as},
    timer::{
        format::{ms_to_readable, split_time_sum},
        Run, TimeType,
    },
};
use regex::Regex;
use std::{fs::File, path::PathBuf};

fn main() {
    let options = NativeOptions {
        ..Default::default()
    };
    eframe::run_native("mystify", options, Box::new(|_| Box::<App>::default())).unwrap();
}

struct App {
    path: PathBuf,
    original: Run,
    temp: Run,
    names: Vec<String>,
    pbs: Vec<String>,
    golds: Vec<String>,
    avgs: Vec<String>,
    title: String,
    cat: String,
    offset: String,
    pb: String,
    first: bool,
    segments: bool,
    selected: Vec<bool>,
}

impl App {
    fn save_run(&mut self) {
        let f = File::options()
            .write(true)
            .create(true)
            .truncate(true)
            .open(&self.path)
            .map_err(|e| fail(e.into()));
        if let Ok(mut f) = f {
            self.temp.to_writer(&mut f).unwrap_or_else(fail);
        }
    }
    fn set_temps(&mut self, original: bool) {
        let r = if original { &self.original } else { &self.temp };
        self.names = r.splits().to_vec();
        self.title = r.game_title().to_string();
        self.cat = r.category().to_string();
        self.pb = ms_to_readable(r.pb().val(), None);
        self.offset = format!("-{}", ms_to_readable(r.offset().val(), None));
        self.pbs = r
            .pb_times()
            .iter()
            .map(|t| ms_to_readable(t.val(), None))
            .collect();
        self.golds = r
            .gold_times()
            .iter()
            .map(|t| ms_to_readable(t.val(), None))
            .collect();
        self.avgs = r
            .sum_times()
            .iter()
            .map(|&(n, t)| ms_to_readable(t / if n == 0 { 1 } else { n }, None))
            .collect();
        self.selected = vec![false; self.names.len()];
        if original {
            self.temp = self.original.clone()
        }
    }
}

impl Default for App {
    fn default() -> Self {
        App {
            path: PathBuf::new(),
            original: Run::empty(),
            temp: Run::empty(),
            names: vec![],
            pbs: vec![],
            golds: vec![],
            avgs: vec![],
            title: String::new(),
            cat: String::new(),
            offset: String::new(),
            pb: String::new(),
            first: true,
            segments: true,
            selected: vec![],
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _: &mut eframe::Frame) {
        if self.first {
            ctx.set_visuals(egui::Visuals::dark());
            set_fonts(ctx);
            self.first = false;
        }
        egui::TopBottomPanel::top("options").show(ctx, |ui| {
            ui.columns(2, |ui| {
                ui[0].vertical(|ui| {
                    ui.horizontal(|ui| {
                        if ui.button("Open MSF").clicked() {
                            if let Some(p) = get_file("Open split file", "*.msf") {
                                self.path = p.into();
                                let f = File::open(&self.path).map_err(|e| fail(e.into()));
                                if let Ok(f) = f {
                                    self.original = Run::from_reader_msf(f).unwrap_or_else(|e| {
                                        fail(e);
                                        Run::empty()
                                    });
                                    self.set_temps(true);
                                }
                            }
                        }
                        if ui.button("Open LSS (experimental)").clicked() {
                            if let Some(p) = get_file("Open split file", "*.lss") {
                                let f = File::open(p).map_err(|e| fail(e.into()));
                                if let Ok(f) = f {
                                    self.original = Run::from_reader_lss(f).unwrap_or_else(|e| {
                                        fail(e);
                                        Run::empty()
                                    });
                                    self.set_temps(true);
                                }
                            }
                        }
                    });
                    ui.horizontal(|ui| {
                        if ui.button("Save").clicked() {
                            if self.path == PathBuf::new() {
                                if let Some(p) = get_save_as() {
                                    self.path = p.into();
                                    self.save_run();
                                }
                            } else {
                                self.save_run()
                            }
                        }
                        if ui.button("Save As").clicked() {
                            if let Some(p) = get_save_as() {
                                self.path = p.into();
                                self.save_run();
                            }
                        }
                    });
                    ui.horizontal(|ui| {
                        let old = self.segments;
                        ui.radio_value(&mut self.segments, true, "Segment Times");
                        ui.radio_value(&mut self.segments, false, "Split Times");
                        if self.segments != old {
                            let (times, gold_times) = if self.segments {
                                (self.temp.pb_times_u128(), self.temp.gold_times_u128())
                            } else {
                                (
                                    split_time_sum(&self.temp.pb_times_u128()),
                                    split_time_sum(&self.temp.gold_times_u128()),
                                )
                            };
                            self.pbs = times.iter().map(|&t| ms_to_readable(t, None)).collect();
                            self.golds = gold_times
                                .iter()
                                .map(|&t| ms_to_readable(t, None))
                                .collect();
                        }
                    });
                    ui.horizontal(|ui| {
                        let fst = first_true(&self.selected[..]);
                        let index = last_true(&self.selected[..]);
                        if ui
                            .button(if fst != usize::MAX {
                                "Add Split Below"
                            } else {
                                "Add Split"
                            })
                            .clicked()
                        {
                            let index = if self.pbs.is_empty() {
                                index
                            } else {
                                index + 1
                            };
                            self.pbs.insert(index, String::from("0.000"));
                            self.golds.insert(index, String::from("0.000"));
                            self.avgs.insert(index, String::from("0.000"));
                            self.names.insert(index, String::new());
                            self.selected.insert(index, false);
                            self.temp.add_split(index);
                        }
                        if ui
                            .button(if fst != usize::MAX && fst != index {
                                "Delete Splits"
                            } else {
                                "Delete Split"
                            })
                            .clicked()
                        {
                            let mut i = 0;
                            while i <= index && i < self.selected.len() {
                                if self.selected[i] {
                                    self.pbs.remove(i);
                                    self.golds.remove(i);
                                    self.avgs.remove(i);
                                    self.names.remove(i);
                                    self.selected.remove(i);
                                    self.temp.remove_split(i);
                                } else {
                                    i += 1;
                                }
                            }
                            self.temp.set_pb(
                                self.temp
                                    .pb_times()
                                    .iter()
                                    .fold(TimeType::None, |acc, v| acc + v.raw()),
                            );
                            self.pb = ms_to_readable(self.temp.pb().val(), None);
                        }
                    });
                    ui.horizontal(|ui| {
                        if ui.button("Clear Times").clicked() {
                            self.temp
                                .set_pb_times(&vec![TimeType::None; self.pbs.len()]);
                            self.temp
                                .set_gold_times(&vec![TimeType::None; self.pbs.len()]);
                            self.temp
                                .set_sum_times(&vec![(1, TimeType::None); self.pbs.len()]);
                            self.temp.set_pb(TimeType::None);
                            self.set_temps(false);
                        }
                        if ui.button("Reset").clicked() {
                            self.set_temps(true);
                        }
                    });
                });
                ui[1].vertical(|ui| {
                    ui.horizontal(|ui| {
                        ui.label("Game Title:");
                        if ui.text_edit_singleline(&mut self.title).changed() {
                            self.temp.set_game_title(&self.title);
                        }
                    });
                    ui.horizontal(|ui| {
                        ui.label("Category:");
                        if ui.text_edit_singleline(&mut self.cat).changed() {
                            self.temp.set_category(&self.cat);
                        }
                    });
                    ui.horizontal(|ui| {
                        ui.label("Offset:");
                        if ui.text_edit_singleline(&mut self.offset).changed() {
                            if let Ok(t) = text_to_ms(&self.offset, true) {
                                self.temp.set_offset(t.into());
                            }
                        }
                    });
                    ui.horizontal(|ui| {
                        ui.label(format!("PB: {}", self.pb));
                    });
                });
            });
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            TableBuilder::new(ui)
                .striped(true)
                .column(Column::exact(35.0))
                .column(Column::remainder().at_least(200.0))
                .columns(Column::remainder().at_least(100.0), 3)
                .header(30.0, |mut header| {
                    header.col(|_| {});
                    header.col(|ui| {
                        ui.label("Split Name");
                    });
                    header.col(|ui| {
                        ui.label("PB Time");
                    });
                    header.col(|ui| {
                        ui.label("Gold Time");
                    });
                    header.col(|ui| {
                        ui.label("Average Time");
                    });
                })
                .body(|mut body| {
                    for i in 0..self.names.len() {
                        body.row(20.0, |mut row| {
                            row.col(|ui| {
                                ui.checkbox(&mut self.selected[i], "");
                            });
                            row.col(|ui| {
                                if ui.text_edit_singleline(&mut self.names[i]).changed() {
                                    self.temp.set_split(i, &self.names[i]);
                                }
                            });
                            row.col(|ui| {
                                if ui.text_edit_singleline(&mut self.pbs[i]).changed() {
                                    if let Ok(time) = text_to_ms(&self.pbs[i], false) {
                                        if self.segments {
                                            self.temp.set_pb_time(i, time.into());
                                        } else if let Ok(last_time) = text_to_ms(
                                            if i == 0 { "0" } else { &self.pbs[i - 1] },
                                            false,
                                        ) {
                                            if time >= last_time {
                                                self.temp.set_pb_time(i, (time - last_time).into());
                                            }
                                        }
                                    }
                                    self.temp.set_pb(
                                        self.temp
                                            .pb_times()
                                            .iter()
                                            .fold(TimeType::None, |acc, v| acc + v.raw()),
                                    );
                                    self.pb = ms_to_readable(self.temp.pb().val(), None);
                                }
                            });
                            row.col(|ui| {
                                if ui.text_edit_singleline(&mut self.golds[i]).changed() {
                                    if let Ok(time) = text_to_ms(&self.golds[i], false) {
                                        if self.segments {
                                            self.temp.set_gold_time(i, time.into());
                                        } else if let Ok(last_time) = text_to_ms(
                                            if i == 0 { "0" } else { &self.golds[i - 1] },
                                            false,
                                        ) {
                                            if time >= last_time {
                                                self.temp
                                                    .set_gold_time(i, (time - last_time).into());
                                            }
                                        }
                                    }
                                }
                            });
                            row.col(|ui| {
                                if ui.text_edit_singleline(&mut self.avgs[i]).changed() {
                                    if let Ok(time) = text_to_ms(&self.avgs[i], false) {
                                        let num = self.temp.sum_times()[i].0;
                                        let total = time * num;
                                        self.temp.set_sum_time(i, (num, total.into()));
                                    }
                                }
                            });
                        });
                    }
                });
        });
    }
}

fn text_to_ms(s: &str, neg: bool) -> Result<u128, ()> {
    lazy_static::lazy_static! {
        static ref TIME: Regex = Regex::new(r"^(\d+:)??(\d+:)?(\d+)?(?:\.(\d{1,3}))?$").unwrap();
        static ref NEG_TIME: Regex = Regex::new(r"^-(\d+:)??(\d+:)?(\d+)?(?:\.(\d{1,3}))?$").unwrap();
    }
    let mut total = 0;
    let caps = if neg {
        NEG_TIME.captures(s).ok_or(())
    } else {
        TIME.captures(s).ok_or(())
    }?;
    if let Some(ts) = caps.get(1) {
        let ts = ts.as_str().strip_suffix(':').unwrap();
        total += ts.parse::<u128>().unwrap() * 3_600_000;
    }
    if let Some(ts) = caps.get(2) {
        let ts = ts.as_str().strip_suffix(':').unwrap();
        total += ts.parse::<u128>().unwrap() * 60_000;
    }
    if let Some(ts) = caps.get(3) {
        total += ts.as_str().parse::<u128>().unwrap() * 1000;
    }
    if let Some(ts) = caps.get(4) {
        let ts = format!("{:0<3}", ts.as_str());
        total += ts.parse::<u128>().unwrap();
    }
    Ok(total)
}

fn last_true(sels: &[bool]) -> usize {
    for (i, &b) in sels.iter().enumerate().rev() {
        if b {
            return i;
        }
    }
    if !sels.is_empty() {
        sels.len() - 1
    } else {
        0
    }
}

fn first_true(sels: &[bool]) -> usize {
    for (i, &b) in sels.iter().enumerate() {
        if b {
            return i;
        }
    }
    usize::MAX
}

fn set_fonts(ctx: &egui::Context) {
    let mut fonts = egui::FontDefinitions::default();
    fonts.font_data.insert(
        "cjk".into(),
        egui::FontData::from_static(include_bytes!("../fonts/NotoSansCJK-Regular.ttc")),
    );
    fonts.font_data.insert(
        "lgc".into(),
        egui::FontData::from_static(include_bytes!("../fonts/NotoSansLGC-Regular.ttf")),
    );
    fonts
        .families
        .entry(egui::FontFamily::Proportional)
        .or_default()
        .push("cjk".to_owned());
    fonts
        .families
        .entry(egui::FontFamily::Proportional)
        .or_default()
        .push("lgc".to_owned());
    ctx.set_fonts(fonts);
}
