# mystify
a graphical tool for creating and modifying mist split files

![mystify screenshot](../../assets/mystify.png)

Allows you to edit every field of a mist split file, as well as add and remove splits. If you mess up, you can
reset the split file to its previous state. You can also create and save new split files from scratch.

Includes experimental support for converting LiveSplit (LSS) files to MSF files. It *usually* works, but has been known to get
tripped up by unusual LSS files or generated ones, so be sure to let me know (and send the lss in question!) if you experience this.
