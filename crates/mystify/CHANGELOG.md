## mystify changelog
### 0.1.0
- initial release
- support modifying all fields of file
- add/remove splits
- open lss
- revert to previously saved version of file
