//! Mist error and result types.
#[cfg(feature = "ser")]
use ron::error::{Error as RonErr, SpannedError};
use std::{io::Error as IoErr, result::Result as StdRes};
use thiserror::Error;

pub type Result<T> = StdRes<T, MistError>;

#[derive(Error, Debug)]
/// Possible errors that can occur in `mist_core`.
pub enum MistError {
    #[cfg(feature = "ser")]
    #[error("could not serialize: {0}")]
    /// Serialization error (for split files, state dumps, config).
    Ser(#[from] RonErr),
    #[cfg(feature = "ser")]
    #[error("could not deserialize file: {0}")]
    /// Deserialization error (for split files, state dumps, config).
    De(#[from] SpannedError),
    #[error("i/o error: {0}")]
    /// I/O error.
    Io(#[from] IoErr),
    #[error("{0}")]
    /// Generic error with a string description.
    Str(String),
}

impl From<&str> for MistError {
    fn from(e: &str) -> Self {
        Self::Str(e.into())
    }
}

impl From<String> for MistError {
    fn from(e: String) -> Self {
        Self::Str(e)
    }
}
