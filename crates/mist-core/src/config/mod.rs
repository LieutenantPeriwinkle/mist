//! Configuration of mist.
mod cfg;
mod colors;
mod font;
mod keybinds;
mod panels;
pub use {
    cfg::get_plugin_dir, cfg::Config, colors::Colors, font::Font, keybinds::KeybindsRaw,
    panels::Panel,
};
