# mist-core

[![Crates.io](https://img.shields.io/crates/v/mist-core)](https://crates.io/crates/mist-core)
[![docs.rs](https://img.shields.io/docsrs/mist-core)](https://docs.rs/mist-core)

The heart of functionality of [mist](https://codeberg.org/periwinkle/mist). Contains utilities specific
to mist as well as some that could be used from other programs to interact with split files.

## usage
Add mist-core to your Cargo.toml.

```toml
[dependencies.mist-core]
version = "2"
```

## features
`mist-core` provides several features, mostly to enable functionality that is only used by the timer. These are:
* `state`: enable the timer::state module, mostly containing things related to mist's internal state during a run
* `dialogs`: enable dialogs module, containing things like dialogs prompting the user for files
* `config`: enable config module, containing the configuration utilities and types for mist
* `portable`: change the configuration path to the directory of the executable rather than platform-specific location
* `instant`: enable timer::instant module, containing an alternate implementation of `std::time::Instant` for platforms where it doesn't act how mist wants it to
* `lss`: enable (experimental, somewhat lossy) conversion of LiveSplit split files to mist split files. most likely to be accurate on files with few or no times set in them.
* `bg`: enable support for mist's background images
* `ser`: enable serialization and deserialization of Run (and StateDump if state is enabled)

`state`, `dialogs`, `config`, `instant` and `ser` are enabled by default, since they are what mist uses by default.

The `Run` struct and associated items are always included.
