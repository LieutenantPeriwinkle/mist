# <img src="assets/MIST.png?raw=true" width="50" height="50"/> mist

<a href="https://liberapay.com/periwinkle/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/A0A5VNCNT)

a minimal, improved speedrun timer

[Changelog](CHANGELOG.md)

[Latest Release](https://codeberg.org/periwinkle/mist/releases/latest)

## features
mist does most of what you would expect your speedrun timer to do as far as timing and operation, but beyond that:
* [X] Cross platform
	* [X] Linux
	* [X] Windows
	* [X] MacOS (builds, at least)
* [X] Human-readable split file (using [ron](https://github.com/ron-rs/ron))
* [X] customizability
	* [X] fonts/font sizes
	* [X] colors
	* [X] keybinds
	* [X] background images
	* [X] panels (sum of best etc)
	* [X] time rounding (any framerate)
	* [X] inline splits or use two rows
* [X] [splits editor](crates/mystify/README.md)
	* [X] convert lss to msf (mostly effective)
* [X] hot reloading
	* [X] split file reloading
	* [X] config reloading
* [X] save and restore current timer state from a file
* [X] plugins

## simple installation
Get the latest release from the [releases page](https://codeberg.org/periwinkle/mist/releases). Linux users will need to install SDL2, SDL2_Image, SDL2\_GFX, SDL2_TTF on their system to run it.
You can also install from crates.io, i.e. `cargo install mist`.

## compiling from source
To build this, you will need SDL2, and SDL2\_TTF as well as potentially SDL2\_Image and SDL2\_GFX depending on your configuration. On Linux, get them from your system
package manager. On macOS, you could use homebrew to install them.

### cargo features
* `bg`: enables timer background images. Requires SDL2\_GFX and SDL2\_Image.
* `icon`: enables app icon. Requires SDL2\_Image.
* `plugins`: enables plugin loading and running.
* `portable`: changes config and plugin directories to be adjacent to the executable rather than platform-specific
* `instant`: enables custom implementation of `std::time::Instant` from `mist-core`.

### windows
Setting up your build on windows kinda sucks. What I did is use [vcpkg](https://github.com/microsoft/vcpkg) to build all of the necessary DLLs and LIBs and
then copied them to the path specified [here](https://github.com/rust-sdl2/rust-sdl2#windows-msvc) in step 3.

Compile with `cargo build --release` then move the exe as well as the sdl related dlls into the same folder to run.

## usage
The default keybinds are:

* <kbd>F1</kbd>: Open new split file
* <kbd>F2</kbd>: Open a new config file
* <kbd>F3</kbd>: Dump timer state to a file
* <kbd>F4</kbd>: Load timer state from a file
* <kbd>Space</kbd>: Start/split/stop
* <kbd>Enter</kbd>: Pause/unpause
* <kbd>Backspace</kbd>: Unsplit
* <kbd>R</kbd>: Reset
* <kbd>RightShift</kbd>: Skip split
* <kbd>&leftarrow;</kbd>: Previous comparison
* <kbd>&rightarrow;</kbd>: Next comparison
* Mousewheel: Scroll splits up/down (if there are more than fit in the window)


## configuration
mist reads its configuration from the user configuration directory, specific to each operating system:
- Linux: `$XDG_CONFIG_HOME/mist/mist.cfg` or `$HOME/.config/mist/mist.cfg`
- Windows: `C:\Users\YourUser\AppData\Roaming\mist\mist.cfg`
- MacOS: `$HOME/Library/Application Support/mist/mist.cfg`

Alternatively, if you have a portable build, mist will look for `mist.cfg` in the directory where the executable is stored.

## about plugins
Plugins are dynamic libraries that mist loads at runtime. You can find information on how to develop plugins [here](crates/mist-pdk/README.md).

If plugins are enabled, they will be loaded from the directory `plugins` in either the platform-specific configuration location
above, or in the executable's directory on a portable build.

This will be eventually a list of interesting plugins but for now there is one:
* [Global hotkeys](https://codeberg.org/vulpesx/mist-global-hotkey)

## Licensing
Like Rust itself, mist is licensed under MIT or Apache 2.0, at your option.
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you shall be dual licensed as above, without any
additional terms or conditions.
